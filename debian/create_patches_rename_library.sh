#!/bin/sh
VENDOR=starlink
LIBNAME=ast

PATCH_SED_CMD="s/lib${LIBNAME}/lib${VENDOR}_${LIBNAME}/g"
FILES="Makefile.am ast_link.in ast.news"

cat > debian/patches/rename_library <<EOF
Author: Ole Streicher <olebole@debian.org>
Last-Update: $(date +%Y-%m-%d)
Description: "${LIBNAME}" is a quite short name for a library that is so 
 specific as this one. Therefore, and for consistency with the "pal" library, 
 "${LIBNAME}" is prefixed by the vendor name "${VENDOR}".
 .
 This patch can be re-created by running 
  ${0}
 on the freshly unpacked source tarball.
$(for f in ${FILES} ; do \
 sed ${PATCH_SED_CMD} < ${f} | diff -u ${f} - --label a/${f} --label b/${f}; \
 done )
EOF
