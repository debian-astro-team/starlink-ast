starlink-ast (9.2.10+dfsg-2) unstable; urgency=medium

  * Team upload.
  * 'd/clean': Add missing extensions. (Closes: #1047627)
  * 'd/control': Update 'Standards-Version' to 4.7.0, no changes required.
  * 'd/rules': Add hardening 'all'.
  * 'd/tests/run-test': Remove useless temporary directory, fixes tests.

 -- Phil Wyett <philip.wyett@kathenas.org>  Sun, 06 Oct 2024 13:21:21 +0100

starlink-ast (9.2.10+dfsg-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + libstarlink-ast-dev: Add Multi-Arch: same.

  [ Ole Streicher ]
  * New upstream version 9.2.10+dfsg
  * Push Standards-Version to 4.6.2. No changes needed
  * Rediff patches

 -- Ole Streicher <olebole@debian.org>  Sun, 25 Jun 2023 20:10:31 +0200

starlink-ast (9.2.9+dfsg-1) unstable; urgency=medium

  * New upstream version 9.2.9+dfsg
  * Rediff patches
  * Adjust starlink.csl for changes in Debian's LaTeX distribution

 -- Ole Streicher <olebole@debian.org>  Mon, 13 Jun 2022 21:37:42 +0200

starlink-ast (9.2.8+dfsg-1) unstable; urgency=medium

  * New upstream version 9.2.8+dfsg
  * Rediff patches
  * Update symbols table

 -- Ole Streicher <olebole@debian.org>  Tue, 10 May 2022 09:37:07 +0200

starlink-ast (9.2.7+dfsg-1) unstable; urgency=medium

  * New upstream version 9.2.7+dfsg
  * Rediff patches

 -- Ole Streicher <olebole@debian.org>  Thu, 13 Jan 2022 11:37:48 +0100

starlink-ast (9.2.6+dfsg-1) unstable; urgency=medium

  * New upstream version 9.2.6+dfsg
  * Rediff patches

 -- Ole Streicher <olebole@debian.org>  Sun, 09 Jan 2022 14:39:49 +0100

starlink-ast (9.2.5+dfsg-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Set field Upstream-Contact in debian/copyright.
  * Set upstream metadata fields: Bug-Submit, Repository,
    Repository-Browse.
  * Drop unnecessary dependency on dh-autoreconf.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata

  [ Ole Streicher ]
  * New upstream version 9.2.5+dfsg
  * Rediff patches
  * Update configure: explicitly build with external cminpack
  * Update symbols table

 -- Ole Streicher <olebole@debian.org>  Tue, 02 Nov 2021 11:56:26 +0100

starlink-ast (9.2.4+dfsg-1) unstable; urgency=medium

  * Team Upload.
  * New upstream version 9.2.4+dfsg
  * Re-diff patches, refreshed
  * Update new symbols in the symbols list
  * d/libstarlink-ast-grf3d9.lintian-overrides: Rename lintian override
  * d/control: Bump standards version to 4.6.0
  * d/watch: Bump watch file version to 4
  * d/{copyright,upstream/metdata,control}: Migrate to https URLs

 -- Nilesh Patra <nilesh@debian.org>  Sun, 05 Sep 2021 17:55:01 +0530

starlink-ast (9.2.3+dfsg-2) unstable; urgency=medium

  * Team Upload.
  * d/rules: Remove .aux and .log files installing
    which is not required
  * d/p/reproducible.patch: make build reproducible
    (Closes: #986738)

 -- Nilesh Patra <nilesh@debian.org>  Sun, 15 Aug 2021 16:50:12 +0530

starlink-ast (9.2.3+dfsg-1) unstable; urgency=medium

  * New upstream version 9.2.3+dfsg. Rediff patches

 -- Ole Streicher <olebole@debian.org>  Tue, 06 Oct 2020 11:53:27 +0200

starlink-ast (9.2.2+dfsg-1) unstable; urgency=medium

  * New upstream version 9.2.2+dfsg. Rediff patches

 -- Ole Streicher <olebole@debian.org>  Mon, 28 Sep 2020 13:49:54 +0200

starlink-ast (9.2.1+dfsg-1) unstable; urgency=low

  * New upstream version 9.2.1+dfsg. Rediff patches
  * Update symbols list

 -- Ole Streicher <olebole@debian.org>  Fri, 18 Sep 2020 15:42:26 +0200

starlink-ast (9.1.3+dfsg-1) unstable; urgency=low

  * New upstream version 9.1.3+dfsg. Rediff patches
  * Push dh-compat to 13
  * Add "Rules-Requires-Root: no" to d/control
  * Add new symbols to symbols file

 -- Ole Streicher <olebole@debian.org>  Thu, 10 Sep 2020 14:22:57 +0200

starlink-ast (9.1.2+dfsg-1) unstable; urgency=low

  * New upstream version 9.1.2+dfsg. Rediff patches

 -- Ole Streicher <olebole@debian.org>  Sun, 14 Jun 2020 15:38:24 +0200

starlink-ast (9.1.1+dfsg-1) unstable; urgency=low

  * New upstream version 9.1.1+dfsg. Rediff patches
  * cme fix dpkg

 -- Ole Streicher <olebole@debian.org>  Mon, 16 Mar 2020 09:00:52 +0100

starlink-ast (9.1.0+dfsg-1) unstable; urgency=low

  * New upstream version 9.1.0+dfsg. Rediff patches
  * Update symbols table
  * Update d/copyright for new version
  * Spellfix in d/control
  * Push Standards-Version to 4.5.0. No changes needed

 -- Ole Streicher <olebole@debian.org>  Sun, 26 Jan 2020 14:30:57 +0100

starlink-ast (9.0.1+dfsg-2) unstable; urgency=low

  * Switch back to unstable to prepare transition
  * Check the values returned by the test
  * Push Standards-Version to 4.4.1. No changes required

 -- Ole Streicher <olebole@debian.org>  Sun, 13 Oct 2019 16:57:17 +0200

starlink-ast (9.0.1+dfsg-1) experimental; urgency=low

  * New upstream version 9.0.1+dfsg. Rediff patches
  * Update symbols table. Add Build-Depends-Package
  * Rename library packages to reflect soversion update
  * Switch to experimental to prepare for transition
  * Add CI tests
  * Override lintian warning on grf3d lib package

 -- Ole Streicher <olebole@debian.org>  Wed, 09 Oct 2019 09:52:17 +0200

starlink-ast (8.7.2+dfsg-1) unstable; urgency=low

  * New upstream version 8.7.2+dfsg. Rediff patches
  * Add gitlab-ci.yml for salsa

 -- Ole Streicher <olebole@debian.org>  Fri, 16 Aug 2019 16:29:35 +0200

starlink-ast (8.7.1+dfsg-1) unstable; urgency=low

  * New upstream version 8.7.1+dfsg. Rediff patches
  * Push Standards-Version to 4.4.0. No changes required.
  * Push compat to 12
  * Add new symbols for libstarlink-ast0

 -- Ole Streicher <olebole@debian.org>  Tue, 30 Jul 2019 18:08:14 +0200

starlink-ast (8.6.3+dfsg-1) unstable; urgency=medium

  * New upstream version 8.6.3+dfsg. Rediff patches
  * Add symbols new in 8.6.3
  * Push Standards-Version to 4.2.1. No changes needed.
  * Push compat to 11

 -- Ole Streicher <olebole@debian.org>  Wed, 26 Sep 2018 11:09:54 +0200

starlink-ast (8.6.2+dfsg-2) unstable; urgency=low

  * Remove Multi-Arch: same from libstarlink-ast-dev (Closes: #874224)

 -- Ole Streicher <olebole@debian.org>  Tue, 16 Jan 2018 11:10:17 +0100

starlink-ast (8.6.2+dfsg-1) unstable; urgency=low

  * Update VCS fields to use salsa.d.o
  * New upstream version 8.6.2+dfsg. Rediff patches, update d/symbols
  * Push Standards-Version to 4.1.3. Change d/copyright format URL to https

 -- Ole Streicher <olebole@debian.org>  Tue, 16 Jan 2018 10:01:41 +0100

starlink-ast (8.4.0+dfsg-1) unstable; urgency=low

  * New upstream version 8.4.0+dfsg
  * Rediff patches
  * Push Standards-Version to 4.0.0. No changes needed

 -- Ole Streicher <olebole@debian.org>  Tue, 20 Jun 2017 08:37:31 +0200

starlink-ast (8.3.0+dfsg-1) unstable; urgency=low

  * New upstream version 8.3.0+dfsg
  * Rediff patches
  * Update libstarlink-ast0.symbols

 -- Ole Streicher <olebole@debian.org>  Thu, 27 Oct 2016 09:52:10 +0200

starlink-ast (8.2.0+dfsg-2) unstable; urgency=low

  * Push Standards-Version to 3.9.8. No changes needed
  * Update symbols file

 -- Ole Streicher <olebole@debian.org>  Sun, 10 Jul 2016 21:06:08 +0200

starlink-ast (8.2.0+dfsg-1) unstable; urgency=medium

  * Add accepted publication
  * Update updatream git repository location
  * Imported Upstream version 8.2.0+dfsg
  * Make header compatible to gbp
  * Rediff patches

 -- Ole Streicher <olebole@debian.org>  Tue, 05 Jul 2016 15:36:17 +0200

starlink-ast (8.1.0+dfsg-1) unstable; urgency=low

  * New upstream version
  * Add upstream/metadata with reference
  * Push standards version to 3.9.7 (no changes)
  * Update VCS fields to use https

 -- Ole Streicher <olebole@debian.org>  Wed, 23 Mar 2016 15:29:20 +0100

starlink-ast (8.0.7+dfsg-1) unstable; urgency=low

  * New upstream version

 -- Ole Streicher <olebole@debian.org>  Mon, 19 Oct 2015 09:46:39 +0200

starlink-ast (8.0.6+dfsg-1) unstable; urgency=low

  * New upstream version

 -- Ole Streicher <olebole@debian.org>  Wed, 14 Oct 2015 10:21:08 +0200

starlink-ast (8.0.5+dfsg-1) unstable; urgency=low

  * New upstream version

 -- Ole Streicher <olebole@debian.org>  Wed, 12 Aug 2015 14:17:36 +0200

starlink-ast (8.0.4+dfsg-1) unstable; urgency=low

  * New upstream version

 -- Ole Streicher <olebole@debian.org>  Tue, 28 Apr 2015 10:35:28 +0200

starlink-ast (8.0.2+dfsg-2) unstable; urgency=low

  * Workaround for armhf FTBS due to a compiler bug.

 -- Ole Streicher <olebole@debian.org>  Sat, 25 Oct 2014 21:34:07 +0200

starlink-ast (8.0.2+dfsg-1) unstable; urgency=low

  * New upstream version
  * Update maintainers e-mail address
  * Update standards version to 3.9.6. No changes needed.

 -- Ole Streicher <olebole@debian.org>  Tue, 21 Oct 2014 12:45:28 +0200

starlink-ast (8.0.0+dfsg-2) unstable; urgency=low

  * Change maintainer and VCS location to debian-astro
  * Add symbols file

 -- Ole Streicher <debian@liska.ath.cx>  Thu, 15 May 2014 10:59:01 +0200

starlink-ast (8.0.0+dfsg-1) unstable; urgency=low

  * New upstream version.
  * Depend on cminpack.

 -- Ole Streicher <debian@liska.ath.cx>  Tue, 13 May 2014 13:32:50 +0200

starlink-ast (7.3.4+dfsg-1) unstable; urgency=low

  * New upstream version

 -- Ole Streicher <debian@liska.ath.cx>  Mon, 10 Mar 2014 10:34:04 +0100

starlink-ast (7.3.3+dfsg-1) unstable; urgency=low

  * New upstream version
  * Push standards version to 3.9.5 (no change needed)

 -- Ole Streicher <debian@liska.ath.cx>  Tue, 07 Jan 2014 10:00:48 +0100

starlink-ast (7.3.2+dfsg-3) unstable; urgency=low

  * Set minimal perl release to fix FTBS caused by bug #731365

 -- Ole Streicher <debian@liska.ath.cx>  Fri, 13 Dec 2013 08:57:45 +0100

starlink-ast (7.3.2+dfsg-2) unstable; urgency=low

  * Re-enable html generation

 -- Ole Streicher <debian@liska.ath.cx>  Thu, 12 Dec 2013 11:29:09 +0100

starlink-ast (7.3.2+dfsg-1) unstable; urgency=low

  * New upstream version
  * Disable html generation due to some bug in latex2html
  * Build with erfa instead of iausofa

 -- Ole Streicher <debian@liska.ath.cx>  Wed, 25 Sep 2013 14:22:59 +0200

starlink-ast (7.3.1+dfsg-1) unstable; urgency=low

  * New upstream version

 -- Ole Streicher <debian@liska.ath.cx>  Fri, 31 May 2013 14:09:15 +0200

starlink-ast (7.2.0+dfsg-1) unstable; urgency=low

  * Change distribution to unstable

 -- Ole Streicher <debian@liska.ath.cx>  Mon, 22 Apr 2013 20:28:57 +0200

starlink-ast (7.2.0+dfsg-1~exp1) experimental; urgency=low

  * New upstream version
  * Push standards version to 3.9.4 (no change needed)

 -- Ole Streicher <debian@liska.ath.cx>  Tue, 16 Apr 2013 09:55:12 +0200

starlink-ast (7.1.1+dfsg-1~exp1) experimental; urgency=low

  * New upstream version

 -- Ole Streicher <debian@liska.ath.cx>  Mon, 10 Dec 2012 10:38:19 +0100

starlink-ast (7.0.6+dfsg-1~exp1) experimental; urgency=low

  * New upstream version
  * Add -lm flag to LDADD
  * Multiarch support

 -- Ole Streicher <debian@liska.ath.cx>  Fri, 17 Aug 2012 15:24:09 +0200

starlink-ast (7.0.4+dfsg-1) unstable; urgency=low

  * New upstream version
  * Set Debian Science Maintainers
  * Set DM-Upload-Allowed

 -- Ole Streicher <debian@liska.ath.cx>  Tue, 12 Jun 2012 10:00:00 +0200

starlink-ast (7.0.3+dfsg-1) unstable; urgency=low

  * Initial release (Closes: #657957)

 -- Ole Streicher <debian@liska.ath.cx>  Thu, 10 May 2012 09:45:00 +0200
